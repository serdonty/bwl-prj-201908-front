# git的原生使用（windows环境）

1. 在git服务器（如：码云）上创建资源仓库（假设仓库名为：scgcxxGit）   
2. 安装Git客户端工具（上官网下载即可）
3. 配置Git的环境变量：path=Git安装目录\cmd
4. 运行Git Bash, 出现命令行界面
5. 在Git Bash命令界面，设置全局配置，创建一个全球用户名、全球邮箱：
    ```
    $ git config --global user.name "你的名字或昵称"
    $ git config --global user.email "你的邮箱" 
    ```
6. 在Git Bash命令界面，将仓库克隆到本地
    ```
    $ cd /f/front #切换到本地仓库存放位置
    $ git clone https://gitee.com/xxx/scgcxxGit.git #将远程资源克隆到本地
    ```

# 使用git命令切换和合并分支示例

1. 进入本地资源库，命令示例如下：
    ```
    cd /E/Project-Dev/front/sl-201908-front
    ```
2. 将当前目录（本地资源库）加入待提交内容
    ```
    $ git add .
    ```

3. 提交修改代码到本地仓库
    ```
    $ git commit -m "修改的东西的描述"
    ```
4. 将本地的 xia_dev 分支推送至远端的 xia_dev 分支，如果没有就新建一个
    ```
    $ git push origin xia_dev
    ```

5. 切换到master_dev主分支上
    ```
    $ git checkout master_dev
    ```

6. 拉取主分支上面的代码
    ```
    $ git pull origin master_dev
    ```

7. 切换到自己的分支
    ```
    $ git checkout xia_dev
    ```

8. 将master_dev分支上的代码合并到当前分支
    ```
    $ git merge master_dev
    ```

9. 将本地的 xia_dev 分支推送至远端的 xia_dev 分支，如果没有就新建一个
    ```
    $ git push origin xia_dev
    ```

# VS code中配置简体中文语言环境和git（windows环境）

1. 下载并安装vscode

2. 配置简体中文语言环境
	> 打开VS code,按组合键Ctrl+Shift+P,搜索language,选择Configure Display Language,设置locale为zh-CN
	> 下载简体中文插件，并启用
3. 配置git
	* 找到Git的安装目录，进入cmd文件夹，拷贝git.exe路径；    
	* 打开VS code 进入设置，搜索“git.path”，复制到用户设置，示例如下：
    ```
    {
        "git.path": "C:/Program Files/Git/cmd/git.exe"            
    }
    ```
4. 上述配置完成后即可在VS code上进行git操作。


# git操作经验
1. 项目负责人（管理员）将项目首次上传git服务器，并以master起点建立master_dev分支，再以master_dev为起点建立各组员分支；
2. 各组员克隆远程仓库，建立本地库，切换到自己的分支；
3. 每天上班
    第一件事：切换到master_dev分支，拉取最新内容，然后切换到自己的分支，将master_dev分支内容合并到自己的分支；
    接下来：在自己的分支上开发，随时可以提交和推送（push）；
    最后一件事：提交和推送（push）自己的分支。
4. 工作告一段落，需要在码云上向主开发分支（master_dev）发出合并请求，由管理员批准实现合并。
5. 注意事项：
    a）分工明确，各组员只在自己的模块文件中开发，不要动别人的东西；
    b）对于公共文件的修改，在修改前要知会其他组员不要动公共文件，而且要确保拉取公共文件的最新内容后再修改。





